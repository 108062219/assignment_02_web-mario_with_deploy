# Software Studio 2021 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description: [hw2-mario](https://assignment-02-web-mario-27125.web.app/)

# Basic Components Description : 
1. World map :


    * In the begining, you will go to the login and sign up page.
    ![](https://i.imgur.com/1Ba2LTC.png)
    * After you have logged in, it will turned to the loading scene.
    ![](https://i.imgur.com/nEDA8Rw.png)
    * After loading, it will automatically jump to the map select page.
    * ![](https://i.imgur.com/MInCqVz.png)
    * You can learn how to play this game. After selecting the map, you will jump to the world map scene.
    * the world map divided into two type, the first is the easier one, which represented by the two pieces of happy cloud.
    * world map 1
    * ![](https://i.imgur.com/cH79PK4.png)
    * world map 2
    * the world map 2 is harder, which represented by the angry block.
    * ![](https://i.imgur.com/C7jtZcJ.png)

2. Player : 
    * the player has two type, which is the smaller and the fatter.
        * smaller
        * ![](https://i.imgur.com/JDe9TRM.png)
        * fatter
        * ![](https://i.imgur.com/09O7mG2.png)
    * the player can move right, left, and jump.
3. Enemies : 
    * there is three type of enemy
        * turtle: If it is dead, it becomes a shell and if you kick it, someone will get hurt.
            * ![](https://i.imgur.com/uIjWJGF.png)
        * goomba (I always call it mushroom): It is the cutest enemy. If you stamp on it, it will flat.
            * ![](https://i.imgur.com/MhSxzFC.png)
        * flower: It live in the pipe, and it won't die.
            * ![](https://i.imgur.com/mfxgeBU.png)


4. Question Blocks : 
    * there are two type of Question block
    * ![](https://i.imgur.com/bH6lnAd.png)
        * with the coin
        * ![](https://i.imgur.com/VPcQZuQ.gif)


        * with the mushroom
        * ![](https://i.imgur.com/AbP17mf.gif)
5. Animations : 
    * the enemy, include of flower, turtle, goomba all have animations
    * the player with smaller and fat both have animations
    
    * for example, the player can smoothly run.
        * the fatter
        ![](https://i.imgur.com/1ylJ3Zh.gif)
        * the smaller
        ![](https://i.imgur.com/lvt2pJL.gif)





11. Sound effects :
    * the bgm except Game Over2 I don't use, I use the other sound effect.
![](https://i.imgur.com/YU2ViJj.png)

13. UI : 
    * it record the current life, current time left, current coin, and the current score of the player, and you can know which world you are now.
    * ![](https://i.imgur.com/3mJJvyw.png)




# Bonus Functions Description : 
1. Angry block :
    * the block is very very angry so it will fall down. If someone who touch it, it will get damage.
    * ![](https://i.imgur.com/XJOcuvF.gif)

2. walk through block: 
    * You can jump onto the block, and you can also walk through it.
